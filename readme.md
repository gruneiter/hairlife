## Список готовых файлов

1. **раздел «участники»**
    - peoples.html — страница раздела
    - peoples-item.html — участник
        - peoples-item-education.html — участник, образование
        - peoples-item-portfolio.html — участник, портфолио
        - peoples-item-galleries.html — участник, фотогалереи
        - peoples-item-gallery.html — участник, фотогалерея
        - peoples-item-video.html — участник, видео

2. **раздел «компании»**
    - companies.html — страница раздела
    - companies-item.html — компания
        - companies-item-staff.html — компания, сотрудники
        - companies-item-services.html — компания, продукция и услуги
        - companies-item-news.html — компания, новости
        - companies-item-discounts.html — компания, акции
        - companies-item-vacancies.html — компания, вакансии

3. **раздел «бренды»**
    - brands.html — страница раздела
    - brands-category.html — категория брендов
    - brands-item.html — бренд

4. **раздел «события»**
    1. раздел «новости»
        - news.html — страница раздела
        - news-item.html — новость
    2. раздел «обучающие мероприятия»
        - seminars.html — страница раздела
        - event.html — мероприятие
    3. раздел «выставки»
        - exhibitions.html — страница раздела
        - exhibitions-archive.html — архив выставок
        - exhibitions-item.html — выставка
    4. раздел «чемпионаты и конкурсы»
        - competitions.html — страница раздела
        - competitions-item.html — элемент раздела
    5. раздел «акции компаний»
        - discounts.html — страница раздела
        - discounts-archive.html — архив акций
        - discounts-item.html — страница акции

5. **раздел «статьи»**
    - blogs.html — страница раздела
    - blogs-category.html — категория статей
    - blogs-post.html — статья

6. **раздел «портфолио»** — ничего

7. **раздел «видео»**
    - videos.html — страница раздела
    - videos-item.html — элемент раздела

8. **раздел «форумы»** — ничего

9. **раздел «товары»**
    - trade.html — страница раздела
    - trade-category.html — категория товаров
    - trade-subcategory.html — подкатегория товаров
    - trade-item.html — товар

10. **раздел «работа»**
    - job.html — страница раздела
    1. раздел резюме
      - job-resumes.html — список резюме
      - resume.html — резюме
    2. раздел вакансий
      - job-vacancies.html — список вакансий
      - vacancy.html — вакансия

11. **раздел «конкурсы»**
    - about-contests.html — страница раздела
    - about-contests-item.html - страница

12. **раздел «объявления»**

13. **раздел «информация»**
    - analytics-posting.html — страница рассылок

14. **раздел «о проекте»**
    - about.html — страница раздела
    - about-terms.html — правила пользования
    - about-faq.html — вопросы и ответы
    - about-news.html — новости портала
    - about-banners.html — баннеры и ссылки
        - about-add-banner-vkontakte.html — как установить баннеры вконтакте
    - contacts.html — контакты
    - cooperation.html — сотрудничество
    - support.html — поддержка

15. **раздел пользователя**
    - reg.html — регистрация
    - login.html — логин

16. **прочее**
    - photos-item.html — страница фотографии
