const path = require('path');
import webpack from 'webpack';

module.exports = {
  mode: 'development',
  entry: {
    main:'./src/scripts/script.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: 'script.js'
  },
  externals: {
    jquery: 'jQuery'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "eslint-loader"
      },
    ],
  }
}
