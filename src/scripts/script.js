import fancybox from '@fancyapps/fancybox'; // eslint-disable-line no-unused-vars
import likely from 'ilyabirman-likely';
import headerMobileSwitcher from '../blocks/header/header-mobile-switcher/header-mobile-switcher';
import headerNavigation from '../blocks/header/header-navigation/header-navigation';
import tabs from '../blocks/tabs/tabs';
import showHide from '../blocks/show-hide/show-hide';
import properties from '../blocks/properties/properties';
import bannerVolosy24 from '../blocks/banner-volosy24/banner-volosy24';
import passRecovery from '../blocks/pass-recovery/pass-recovery';
import formRentElems from '../blocks/form-rent-elements/form-rent-elements';

headerMobileSwitcher();
headerNavigation();
likely.initiate();
showHide();
tabs();
properties();
bannerVolosy24();
passRecovery();
formRentElems();
