export default function passRecovery() {
  const formLogin = document.querySelector('.login-form');
  const formRecovery = document.querySelector('.form-recovery');
  if (formLogin) {
    formLogin.login.addEventListener('change', (e) => {
      if (e.target.value !== '') {
        localStorage.setItem('loginForRestore', e.target.value);
      } else {
        localStorage.removeItem('loginForRestore');
      }
    });
  }
  if (formRecovery) {
    const login = localStorage.getItem('loginForRestore') || undefined;
    if (login) formRecovery.login.value = login;
  }
}
