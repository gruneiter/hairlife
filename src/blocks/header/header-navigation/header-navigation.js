function headerNavigationSublistSwitcher() {
  const hnss = Array.from(document.querySelectorAll('.header-navigation__sublist-switch'));
  hnss.forEach((item) => {
    item.addEventListener('click', (e) => {
      e.target.classList.toggle('header-navigation__sublist-switch--active');
      e.target.nextElementSibling.classList.toggle('header-navigation__sublist--active');
    });
    return item;
  });
}

function headerNavigationSublistPosition() {
  const subMenus = Array.from(document.querySelectorAll('.header-navigation__sublist'));
  subMenus.forEach((menu) => {
    const prntMenu = menu.parentNode.parentNode;
    const menuPos = menu.getBoundingClientRect().x + menu.getBoundingClientRect().width;
    const prntPos = prntMenu.getBoundingClientRect().x + prntMenu.getBoundingClientRect().width;
    if (menuPos > prntPos) {
      menu.classList.add('header-navigation__sublist--right');
    }
  });
}


export default function headerNavigation() {
  headerNavigationSublistSwitcher();
  headerNavigationSublistPosition();
}
