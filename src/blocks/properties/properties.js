function propertiesPopup() {
  const popupBlocks = Array.from(document.querySelectorAll('.properties__popup-name'));
  popupBlocks.forEach((item) => {
    item.addEventListener('click', (e) => {
      const nextSibling = e.currentTarget.nextElementSibling;
      if (nextSibling.classList.contains('properties__popup-body')) {
        nextSibling.classList.toggle('properties__popup-body--active');
      }
    });
    return item;
  });
}

export default function properties() {
  propertiesPopup();
}
