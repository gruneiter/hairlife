const subscribeModule = () => {
  const module = document.querySelector('.subscribe-module');
  module.addEventListener('click', (e) => {
    if (e.target.classList.contains('subscribe-module__open')) {
      e.preventDefault();
      module.classList.toggle('subscribe-module--active');
    }
  });
};

export default subscribeModule;
