tinyMCEPopup.requireLangPack();

var YouTubeDialog = {
	init : function() {
	},

	insert : function() {
		
		var src = document.forms[0].youtubeLINK.value;
		var flash_mov = "";

		if( src.indexOf("https://youtu.be/") > -1 ) {
			src = src.replace("https://youtu.be/", "");
			src = "https://www.youtube.com/embed/"+src;
		}
		else {
			if( src.indexOf("https://www.youtube.com/watch?v=") > -1 )
				src = src.replace("https://www.youtube.com/watch?v=", "");
			if( src.indexOf("https://www.youtube.com/v/") < 0 )
				src = "https://www.youtube.com/embed/"+src;
		}
		
		flash_mov = src;
		
		// Insert the contents from the input into the document
		var embedCode = '<br /><br /><iframe width="'+document.forms[0].youtubeWidth.value+'" height="'+document.forms[0].youtubeHeight.value+'" src="' + flash_mov + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe><br /><br />';
		tinyMCEPopup.editor.execCommand('mceInsertRawHTML', false, embedCode);
		tinyMCEPopup.close();
	}
};

tinyMCEPopup.onInit.add(YouTubeDialog.init, YouTubeDialog);
