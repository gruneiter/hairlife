tinyMCE.init({
	language : "ru",
	mode : "textareas",
	theme : "advanced",
	forced_root_block : false,
	force_br_newlines : true,
	force_p_newlines : false,
	invalid_elements : "font",
	cleanup : true,
	convert_urls : false,
	theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,bullist,numlist,|,undo,redo,|,link,unlink,|,code",
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_path_location : "bottom",
	extended_valid_elements : "a[name|href|target|title|onclick]"
});