tinyMCE.init({
	language : "ru",
	forced_root_block : false,
	force_br_newlines : false,
	force_p_newlines : true,
	mode : "textareas",
	theme : "advanced",
	valid_elements : ""
	+ "a[href|target|title],strong/b,em/i,strike,u,"
	+ "#p,-ol[type|compact],-ul[type|compact],-li,br[clear],"
	+ "img[src|border|alt=|title|width|height|align],"
	+ "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width"
	+ "|height|src|*],h2,h3,h4,h5,tr,td,th,iframe[|*]",
	cleanup : true,
	convert_urls : false,
	theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,bullist,numlist,|,undo,redo,|,link,unlink,|,code,youtube,pagebreak,image,insertimage,pasteword,formatselect",
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_path_location : "bottom",
	theme_advanced_blockformats : "h2,h4,blockquote",
	plugins : "youtube, pagebreak, media, paste, advimage",
	media_strict: true,
	pagebreak_separator : "<!-- cat -->"



});